
objs=main.o init_conditions.o output.o diffs.o
cc=gcc
bin=shock
libs= -lm 
cflags= -g

all: $(objs)
	$(cc) $? -o $(bin) $(libs) $(cflags)

%.o: %.c
	$(cc) -c -o $@ $< $(cflags)

clean:
	rm *.o  $(bin) 
