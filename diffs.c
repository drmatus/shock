#include <math.h>

//Usando el metodo que aparece en las diapos, calcular la derivada para cada x.
//Los parametros y_1 e y_n1 corresponden a las condiciones de borde, mientras
//que dx es el intervalo espacial
//Las derivadas son almacenadas en el arreglo dy, el cual debe tener memoria
//asignada
void *diff_x(float *y, int n, float y_1, float y_n1, float dx, float *dy){
    int i;
    float h = 1.0 / (2*dx);

    for(i=0; i<n; i++){
        if(i==0){
            dy[i] = y_1 - y[1];
        }
        else if(i==(n-1)){
            dy[i] = y[i] - y_n1;
        }
        else{
            dy[i] = y[i-1] - y[i+1];
        }
        dy[i] *= h;
    }
}

//Calcular drho/dt
void *diff_rho(float *rho, float *drho_x, float *v, float *dv_x, int n, float *drdt){
    int i;
    for(i=0; i<n; i++){
        drdt[i] = -v[i] * drho_x[i] - rho[i] * dv_x[i];
    }
}

//Calcular dv/dt
void *diff_v(float *rho, float *drho_x, float *v, float *dv_x, float A, float gamma, int n, float *dvdt){
    int i;
    for(i=0; i<n; i++){
        dvdt[i] = -v[i] * dv_x[i] - A*gamma*pow(rho[i],-1.0/3.0) * drho_x[i];
    }
}

//Actualizar los valores de y, usando la derivada temporal dy y el intervalo de
//tiempo dt
void integrate(float *y1, float *y, float *dy, float dt, int n){
    int i;
    for(i=0; i<n; i++){
        y1[i] = y[i] + dy[i] * dt;
    }
}
