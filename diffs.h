
void *diff_x(float *y, int n, float y_1, float y_n1, float dx, float *dy);

void *diff_rho(float *rho, float *drho_x, float *v, float *dv_x, int n, float *drdt);

void *diff_v(float *rho, float *drho_x, float *v, float *dv_x, float A, float gamma, int n, float *dvdt);

void integrate(float *y1, float *y, float *dy, float dt, int n);
