#include <stdlib.h>
#include <math.h>

//Esta funcion crea un arreglo con n elementos, cuyos valores van de 0 hasta L
//La funcion tambien calcula el ancho de la grilla, valor almacenado en la
//variable "dx", la cual debe tener memoria asignada
float *init_x(float L, int n, float *dx){
    int i;
    float *ret;
    
    //Se asigna memoria al arreglo de posiciones
    ret = (float *) malloc(sizeof(float) * n);

    //Se calcula el ancho de la grilla
    *dx = L/n;

    //Cada elemento del arreglo de posiciones tiene el valor i*dx, de esta
    //forma el arreglo va desde 0 hasta L-dx
    for(i=0; i<n; i++){
        ret[i] = (*dx)*i;
    }

    return ret;
}

//Aca se crean los valores iniciales para la densidad 
//los primeros n/2 elementos tienen el valor "rho", mientras que los n/2
//elementos siguientes tienen una densidad de rho*1e-4
float *init_rho(float rho, int n){
    int i;
    float *ret;
    float dr;

    //Se asigna memoria al arreglo de densidades
    ret = (float *)malloc(sizeof(float) * n);

    //Se asignan valores al arreglo de densidades
    for(i=0; i<n/2; i++){
        //Los primeros n/2 elementos tienen densidad rho
        ret[i] = rho;
        //Los segundos n/2 elementos tienen densidad rho*1e-4
        ret[i + n/2] = rho * 1e-4;
    }

    return ret;
}

//Esto es para crear un arreglo de largo n, lleno de ceros
float *zeros(int n){
    int i;
    float *ret;
    
    ret = (float *)malloc(sizeof(float)*n);
    for(i=0; i<n; i++) ret[i] = 0;

    return ret;
}
