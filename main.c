#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "init_conditions.h"
#include "output.h"
#include "diffs.h"

//Dados los valores de la densidad, calcular la presion en cada elemento de la
//grilla.
//Los valores son almacenados en el arreglo P, el cual debe tener memoria
//asignada
float Presion(float *rho, float *P, int n, float gamma){
    int i;
    float A, T, k_B, m_H, rho_0;

    T   = 10;       //K
    k_B = 1.38e-16; //gr (cm s-1)**2/K
    m_H = 1.67e-24; //gr
    rho_0 = 1e-20;  //gr cm-3
    
    //Calcular la constante de normalizacion para la ecuacion de estado
    A = pow(rho_0, 1-gamma) * T *k_B /m_H;

    //Mediante la ec. de estado, calcular la presion
    for(i=0; i<n; i++){
        P[i] = A * pow(rho[i], gamma);
    }
    return A;
}

//Velocidad del sonido, para un gas ideal
void sound_speed(float gamma, float *P, float *rho, float *c, int n){
    float *ret;
    int i;
    for(i=0; i<n; i++){
        //Esta formula la saque de wikipedia
        c[i] = sqrt(gamma * P[i]/rho[i]);
    }
}

//El largo del timestep es calculado aqui
float timestep(float *v, float *c, int n, float dx, float epsilon){
    float dt, vel_mayor, min;
    int i;

    dt = dx*epsilon;
    //Se toma el maximo entre la velocidad del sonido y la velocidad del gas
    //del primer elemento de la grilla.
    min = 1.0 / ((c[0] > v[0])?c[0]:v[0]);

    for(i=1; i<n; i++){
    //Elegir el mayor entre la velocidad del gas y la velocidad del sonido
    //del i-esimo elemento de la grilla
        vel_mayor = (c[i] > v[i])?c[i]:v[i];
    //si este es menor que el elemento anterior ("min"), guardar 1/vel_mayor
    //como el nuevo "min"
        if (1.0/vel_mayor < min)
            min = 1.0/vel_mayor;
    }

    //Finalmente, el timestep es el mas pequeño de los valores calculados,
    //multiplicado por dx*epsilon
    return min*dt;
}

int main(){
    float *rho, *x, *v, *P, *c;
    float *rho1, *v1;
    float *drho_x, *dv_x;
    float *drho_t, *dv_t;
    float L, dx;
    float rho_0;
    float t, dt, t_end, epsilon;
    float A,gamma;
    int i, n, step;
    int predictor;


    //Los parametros que dio el profe:
    //Largo del sistema
    L = 3e15; //cm
    //Numero de puntos de la "grilla"
    n = 30;
    //Densidad inicial
    rho_0 = 1.0e-20; //gr cm-3
    //coeficiente adiabatico
    gamma = 5.0/3.0;

    //Esta variable es para el archivo de salida, indica el numero del timsetep
    step=0;

    //Si esta variable es 0, entonces usar euler explicito.
    //Si es 1, entonces usar 1 predictor y 1 corrector
    predictor = 0;

    // Print_init("salida");

    //Inicializando densidad y posisiones
    rho = init_rho(rho_0, n);
    x = init_x(L, n, &dx);

    //Asignando memoria a los arreglos necesarios
    rho1   = zeros(n);
    v      = zeros(n);
    v1     = zeros(n);
    P      = zeros(n);
    c      = zeros(n);
    drho_x = zeros(n);
    dv_x   = zeros(n);
    drho_t = zeros(n);
    dv_t   = zeros(n);

    //Presion y velocidad del sonido iniciales
    Presion(rho, P, n, gamma);
    sound_speed(gamma, P, rho, c, n);

    t = 0;
    //El tiempo total de la simulacion es el largo del sistema, dividido por la
    //velocidad del sonido
    t_end = L / c[1] ;

    //Se imprime el tiempo total de la simulacion
    printf("t_end = %g\n", t_end);

    //epsilon es un parametro que ajusta el tamaño del timestep. Mientras mas
    //pequeño, mas tiempo es requerido para finalizar la simulacion
    epsilon = 0.01;

    //imprimir condiciones iniciales
    Print_data(step, t, x, rho, v, P, n);

    //Aca va el loop para cada timestep
    //Mientras el tiempo actual sea menor que el timepo final, calcular los
    //valores de la presion, densidad y velocidad para t+dt:
    while(t < t_end){
    
        //Dadas las condiciones actuales, calcular la presion, velocidad del
        //sonido y el largo del timestep:
        A = Presion(rho, P, n, gamma);
        sound_speed(gamma, P, rho, c, n);
        dt = timestep(v, c, n, dx, epsilon);
        // printf("dt = %g\r", dt);
        // fflush(stdout);

        //Derivadas espaciales:
        //drho/dx:
        diff_x(rho, n, rho_0, rho_0*1e-4, dx, drho_x);
        //dv/dx:
        diff_x(v, n, 0, 0,dx, dv_x);

        //Derivadas temporales:
        //drho/dt
        diff_rho(rho, drho_x, v, dv_x, n, drho_t);
        //dv/dt
        diff_v(rho, drho_x, v, dv_x, A,gamma, n, dv_t);

        //Avanzar rho y v en dt:
        integrate(rho, rho, drho_t, dt, n);
        integrate(v,   v,   dv_t,   dt, n);

        // Si rho < 0, reemplazar rho=0
        
        for (i=0; i<n; i++){
            if(rho[i] < 0.0) rho[i] = 0.0;
        }
        t += dt;

        step++;

        //Imprimir 1 de cada 10 timesteps
        if(!(step%10))
            Print_data(step, t, x, rho, v, P, n);

    //Fin del loop
    }

    printf("\nstep_end = %d\tt_end = %f\n", step,t);


    //Liberar la memoria de los arreglos
    free(rho);
    free(rho1);
    free(x);
    free(v);
    free(v1);
    free(P);
    free(c);
    free(drho_x);
    free(dv_x);
    free(drho_t);
    free(dv_t);

    // Print_close();
    return 0;
}
