#include <stdio.h>

FILE *out;

//Esta funcion abre el archivo y escribe un header con el significado de cada
//columna
void Print_init(char *filename){
    out = fopen(filename, "w");

    fprintf(out, "#n_puntos t[Myr]\n");
    fprintf(out, "#x[cm] rho[gr cm-3] v[cms] P[erg cm-3]\n");
}

void Print_data(int step, float t, float *x, float *rho, float *v, float *P, int n){
    int i;
    FILE *out;
    char nombre[100];

    sprintf(nombre, "salida_%06d.dat", step);

    out = fopen(nombre, "w");

    fprintf(out, "#n_puntos t[s]\n");
    fprintf(out, "#x[cm] rho[gr cm-3] v[cms] P[erg cm-3]\n");

    
    //Header con el tiempo y numero de lineas
    fprintf(out, "%d %d %f\n", step, n, t);

    //Imprimir tabla al archivo
    for(i=0; i<n; i++){
        fprintf(out, "%g %g %g %g\n", x[i], rho[i], v[i], P[i]);
    }
    //Dejar un espacio en blanco para no confundir cada snapshot
    fprintf(out, "\n");

    fclose(out);
}

/*Cerrar el archivo
 *    Ya que esta funcion contiene una sola linea, perfectamente podria ir en
 *    main(), pero preferi dejarla aparte en caso de que haya que hacer mas
 *    cosas al momento de cerrar el archivo.
 */
void Print_close(){
    fclose(out);
}

